import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    public static int height(int cycle){
        int h = 1;
        if(cycle==0) 
        return h;
        while(cycle>0){
            if(cycle>0){
                h = h*2;
                cycle--;
            }
            if(cycle>0){
                h = h+1;
                cycle--;
            }
        }
        return h;
    }
    
 public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
 
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        for(int i=0;i<test;i++){
            int cycle = in.nextInt();
            System.out.println(height(cycle));
        }
    }
 
}   
